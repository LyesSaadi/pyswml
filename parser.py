"""
This file contains all the code used to parse a SWML file.

If you seek to compile your code, this is certainly the
wrong file, @see compile.py. But if you seek to hack the
parser, welcome home!

Here be dragons, Hic sunt dracones!

# Main function: parse(str)

# Shortcut functions

- parse_from_list(list)
- parse_from_file(file)

# Parsing functions

- identify_indentation(self)
- remove_indentation(self)
- get_structure_skeleton(self)
"""
import _io
import re


class Parser:
    """
    Main parser for SWML.

    It contains all the functions related to the parsing of a SWML file.
    You should only use the __init__ function unless you are hacking/making
    changes to the Parser.

    The Journey of A Compiled SWML File:
    [0 - SWML Preprocessor.]
    1 - Parser: This file.
    2 - Dommifier: @see dom.py.
        a - Base dom: built-in.
        b - Extensions: @see documentation (overrides "a" in case of conflict).
    3 - Importer: @see import.py.
        a - Base libraries: @see libs/*.py.
        b - User libraries (overrides "a" in case of conflict).
    4 - Error Checker: @see checker.py.
    [4b - Debugger: @see debugger.py.]
    [5 - SWML Postprocessor.]
    """

    def __init__(self, inp):
        """
        Choose the right shortcut function according to input type.

        The idea behind decomposing this function into other shortcut
        functions is enabling more complex treatment of a certain input or
        easier hacking in case someone needs to.

        It automatically detect the type of input that you provide him,
        - If it is a string -> self.parse()
        - If it is a list -> self.parse_from_list()
        - if it is a file (_io.TextIOWrapper) -> self.parse_from_file()
        """
        t = type(inp)
        if t == str:
            self.parse(inp)  # If the input is a string
        elif t == list:
            self.parse_from_list(inp)  # If the input is a list
        elif t == _io.TextIOWrapper:
            self.parse_from_file(inp)  # If the input is a file
        else:
            raise Exception("[PySWML]/Parser/Input Error: " +
                            "(0;0) Input Type Unknown.")

    # Main function
    def parse(self, s):
        """
        Parse a provided string containing SWML code.

        It's only purpose is to execute the parsing functions in order while
        checking for syntax errors.

        This function require a string which represent the code to parse and
        output two lists representing the structure of the code.
        """
        self.structure = s.split("\n")  # Splitting the file into a list.
        self.identify_indentation()  # Identifying the indentation type.
        self.remove_indentation()  # Removing the indentation from the file.
        # And creating a list gathering all indentation levels in the file.
        self.output = self.get_structure_skeleton()  # Merging the lists.
        # Creating therefore a basic structure, a basic DOM.

    def identify_indentation(self):
        """
        Identify the file indentation.

        The indentation in a SWML file should be either spaces or tabs. It is
        recommended to use the same indentation as Python (4 spaces). You can
        use as much spaces or tabs that you want. It should only be consistent
        in the entire file and not using tabs and spaces at the same time.

        It creates two variables, a string and a list. The string contains the
        indentation. And the list contains the indentation levels, in order.
        """
        exp_isolate = re.compile(r"^(\s*).*")  # Regex used to isolate
        # the indentations in each line.
        exp_get_first_inden = re.compile(r"^(\s+)$")  # Regex used to
        # extract the first indentation.
        self.exp_identify_empty_lines = re.compile(r"^\s*$")  # Regex used to
        # identify first lines.
        file = []
        for s in self.structure:  # Reading each line
            # Extracting it's indentation
            if not re.match(self.exp_identify_empty_lines, s):
                file.append(re.sub(exp_isolate, r"\1", s))
            else:
                file.append("")
        for s in file:  # Reading each line
            # Identifying first indentation
            if re.match(exp_get_first_inden, s):
                # Saving the indentation type and breaking the loop
                self.i_type = s
                break
        self.indentation = []
        for s in file:  # Reading each indentation of the file
            # Counting the level of indentation
            self.indentation.append(s.count(self.i_type))

    def remove_indentation(self):
        """Remove the indentation at the beginning of each line."""
        # Regex to extract the indentation leaving every other thing.
        exp_extract_indentation = re.compile(r"^(?:{i})*(\S+)"
                                             .format(i=self.i_type))
        # Reading the file
        for i, s in enumerate(self.structure):
            # Extracting all indentation, leaving only substance
            if re.match(exp_extract_indentation, s):
                self.structure[i] = re.sub(exp_extract_indentation, r"\1", s)
            # If an issue happened, verifying if it's simply an empty line
            # or crashing.
            else:
                if not re.match(self.exp_identify_empty_lines, s):
                    raise Exception("[PySWML]/Parser/Indentation Error: " +
                                    "(" + str(i + 1) +
                                    ";0) Inconsistent indentation.")

    def get_structure_skeleton(self):
        """Merge the two lists."""
        f = []
        # Creating a basic structure using this form :
        # (Line number, Indentation level, Code)
        for i, s in enumerate(self.structure):
            # Being sure that the line isn't empty
            if not re.match(self.exp_identify_empty_lines, s):
                f.append((i, self.indentation[i], s))

    # Shortcut functions
    def parse_from_file(self, file):
        """Shortcut function to parse a file."""
        with file:
            self.parse(file.read())

    def parse_from_list(self, l):
        """Shortcut function to parse a python list."""
        self.parse("\n".join(l))
